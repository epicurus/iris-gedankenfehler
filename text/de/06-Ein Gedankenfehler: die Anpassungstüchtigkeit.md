## Ein Gedankenfehler: die Anpassungstüchtigkeit

<div class="handwritten">
So, du meinst, dass unsere jetzige Art, Zeit zu denken, auch nur ein Gedankenfehler ist?
</div>

Ja, im höchsten Maße.
Seitdem diese Denkweise da ist, da ist die Unbefriedigtheit gewachsen wie eine Lawine, und die Menschen fühlen sich nicht ausreichend, fühlen sich wertlos, weil sie ihre Zeit nicht optimal einsetzen können, sondern sie brauchen Schlaf Essen und alles andere Notwendige.
Und sie wollen alles kaufen, was man kaufen kann.
Zum Beispiel Zeit kaufen dadurch, dass jemand putzt und Essen macht und alle andere praktische Arbeit macht, so dass ich Zeit habe andere, wesentliche Dinge zu tun.

Menschen, die dann zu Produkten werden, anstatt, dass ich es selber mache, und dadurch ich Zeit gewinne.
Das bewirkt, dass du nicht deinen Körper als du selber lebst.
Und das tust, was du tun kannst, verantwortlich und frei wirst.
Du wirst dann unzufrieden und verstehst nicht warum.
Und da machst du mehr und mehr von dem, was du wesentlich findest und du wirst mehr und mehr müde und  bekommst Burnout daran.
Das Leben fühlt sich sinnlos an und dass Ich fühlt sich ungenügend an, nicht tauglich an.

Das ist der Preis, den wir zahlen müssen, für die gewonnene Zeit.
Die Unzufriedenheit des Körpers, nicht bestätigt zu werden durch das, was wir tun können, und tun.
Wenn wir in diesem Gedankenfehler hängen bleiben, dann gewinnen wir Zeit, aber wir gewinnen nicht innere Zufriedenheit, hier und jetzt, in uns selber.

Das Bedürfnis des Körpers, dass wir tun, was wir können, das, was wir entwickelt haben und gelernt haben, was bewirkt, dass wir verantwortlich leben und frei sind, das geht verloren.
Dann werden Schlaf, Essen, äußere Geborgenheit und die Berührung durch die Sinne, diese werden schlecht und wie werden gestresst dadurch und unzufrieden, frustriert, irritiert.
Und wir reiben uns an uns selber auf.
Ist gleich: Es geht uns nicht gut.
Ist gleich: geht uns schlecht.
Ist gleich: Wir wollen Medikamente damit es uns gut geht.
Wir rennen herum im Hamsterrad.

Als ich aufwuchs in dieser Zivilisation, schaute man auf die Zeit als auf eine Goldgrube.
Du, die du jung bist, und wo die Zeit, wo die Welt vor deinen Füßen liegt, anders ist es bei uns, die wir alt sind, wir können nicht viel tun.
Aber wir sind trotzdem zufrieden.

So entstand das Ideal der Anpassungstüchtigkeit.
Und diese gehörte so hin, das wichtigste Meritum, in der Schule sein, Noten, im Benehmen und in Ordnung.
Wenn die eigenen Noten darin schlecht waren, wenn ein Mensch nicht große Einsen in beiden Fächern hatten,dann bekam man keine Arbeit, wenn man mit der Schule fertig war, und man konnte keine höhere Bildung anfangen, obwohl man einen Kopf hatte, der gut dafür war.
Man hat auf einen solchen Menschen herabgeschaut.

Wenn dir wohlerzogen sind, wenn wir tüchtig, schnell und effektiv, rationell sind, dann, dann werden unsere Eltern stolz auf uns, denn dann haben sie es richtig getan.
Und sie haben uns dazu gebracht, wohlgeartete Produkte zu sein.
Es ist ein Gedankenfehler, der zu einem Majoritätsmissverständnis geworden ist, und der uns hemmt und begrenzt für den Rest unseres Lebens.
Es sei denn wir waschen es frei von Bewertungen.
Wie wenn das der einzige Weg wäre, dass es uns gut geht.
Die Wirklichkeit besagt was anderes.
