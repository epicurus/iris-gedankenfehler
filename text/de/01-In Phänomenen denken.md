## In Phänomenen denken

<div class="handwritten">
Iris, jetzt haben wir so viel geredet und so viele Male sagst du, dass etwas ein Gedankenfehler sei, und deshalb möchte ich alle meine Fragen beantwortet haben zu diesem Thema.
Erstens: Warum nennst du es Gedankenfehler?
</div>

Wenn ich Gedankenfehler sage, dann meine ich, wenn es eine Vorstellung gibt, die bewirkt, dass du selber nicht zufrieden sein kannst, dass du in dir Konflikte bekommst und du wirst geladen, sobald du darüber reden sollst, oder sobald du hörst, dass jemand anderes etwas darüber sagt.
Wenn du sagst: „sag das nicht zu mir, denn ich werde nur wütend und finde dass du ein Dummkopf bist.“
Auch wenn du nur so denkst, dann weißt du, dass du da mit einem Konflikt hast.
Ohne darüber reflektiert zu haben, meinst du dass du einen Konflikt mit der Auffassung der anderen Person hast.
Und darin besteht der erste Gedankenfehler.
Das ist das, was in dir reibt, Geräusche macht und womit du einen Konflikt hast.
Das versuchst du zu behüten und versuchst, es so zu machen, dass es nicht falsch wird, dass du dann deine Denkweise ändern müsstest.

Wenn du jemanden überzeugen brauchst und es beweisen brauchst, dann ist es eine Bewertung und nicht ein Phänomen, was den Ausgangspunkt darstellt.
Aus dem kannst du verstehen, dass es sekundär ist und etwas was du dir ausgesucht hast.
Es ist nicht etwas, was nur an sich selber da ist, was du in Worte kleidest, zu dem du Überlegungen anstellst.
Etwas, wo du versucht zu sehen, ob es sich so verhalten kann, oder, um ein anderes Wort dafür zu gebrauchen, ob es anders sein könnte.
Ob es etwas ist, was interessant ist, hin und her zu drehen, darüber zu reden und dies mit anderen zu teilen.
Ein Phänomen, was nur das ist, was es ist.
Nicht richtig oder falsch, gut oder schlecht, oder was anderes, was in dualistischer Bewertung ist.
Sondern etwas, was zu der bereichernden Begegnung wird, und was bei dir bleibt als ein Sich-Fragen innerhalb dir selber und innerhalb von anderen.

Wenn etwas nicht ein Gedankenfehler ist, dann ist es etwas, was sich klärt und sich zurechtlegt.
Die meisten Dinge, worüber wir miteinander reden, mit ihnen haben wir keine Probleme, und so entsteht keine Ladung dabei.
Entweder ist es etwas, worüber wir reden, um eine gemeinsame Lösung herauszufinden, oder wir erzählen anderen Menschen etwas, oder wir teilen uns ein Erlebnis und so weiter.
Bei einem Miteinander-Sprechen verändert sich die Auffassung von beiden ein wenig, da dieses das vom anderen in das eigene einverleibt.
Das ist das, was man Beiderseitigkeit nennt, das miteinander sprechen mutualerweise.

Wenn es ist wie es ist, unvollständig und ein wenig unklar, und manchmal löst sich das auf und ändert sich, dann ist es frei, dann mache (baue???) ich das zu meinem eigenen um und bin zufrieden bevor und währenddem es vor sich geht.
Und dann wird es gut genug.
Dann bin ich ich selber als ich selber, in dem, was da ist, wenn nichts anderes da ist.

Ich habe einen Körper und im Körper bin ich ich selber.
Ich habe einen Bewusstsein und das Bewusstsein gibt es ohne Substanz oder gibt es als ein Wissen innerhalb von einem, unreflektiert, und draußen um mir (mich???) herum und in dem Kontakt mit anderen und durch meine Sinne reflektiere ich über alles und über alle, die es da gibt meinem Bewusstsein.

Wir beide sind uns wohl bewusst, dass nichts absolut ist, sondern dass alles relativ und in Bewegung ist und sich über die Zeit hin ändert, dass die Zukunft nur eine Vorhersage ist.
Und wir selber brauchen nur eine Richtung, in der wir gerne unterwegs sind.
