## Ein Gedankenfehler: Hierarchien statt Rangordnung

<div class="handwritten">
Warum ist es dann nicht so, dass wir nicht immer auf die andere Weise tun, dass wir diesen Gedankenfehler   nicht brechen, wenn wir eine andere Weise mit im Gepäck haben?
Und warum lernen wir nicht als Erwachsene und funktionieren ohne Angst?
Sondern wir funktionieren irrational, trotz des Bewusstseins, dass dies nicht richtig ist.
Das ist so verkompliziert und unpraktisch angepasst.
So schlecht für uns selber.
</div>

Einer der wichtigsten Gründe ist, dass einige besser sein wollen als andere.
Dass ein Unterschied bestehen soll zwischen Armen und Reichen.
Und dass die Reichen dann auf eine moderne Weise tun wollen, die abweicht von dem Natürlichen, um Aufmerksamkeit zu erzeugen, damit sie besonders werden.

Wir schaffen natürlicherweise eine Rangordnung, um unseren Platz in der Herde zu finden.
Und dieser Platz kann unterschiedlich sein über die Zeit hin.
Das hängt von vielen Dingen ab, vom Alter, von der Stärke, vom Geschlecht, von der Lebenserfahrung und anderem.
Aber wenn Menschen diese zu einer Hierarchie machen, wie wenn es bessere und schlechtere Menschen gibt, und dass sie unter unterschiedlichen Bedingungen leben sollen, unterschiedliche Gehälter haben sollen, unterschiedliche Vorteile und Nachteile, da entsteht Konkurrenz, Einsamkeit und Ausscheiden.
Dann wird es bedingt, anstatt wie es eigentlich ist, das Finden des eigenen Platzes in der Gruppe.
Bei Hierarchien ist es nötig mit einem Diktator, der von oben her die Bedingungen diktiert.
Während bei der Rangordnung herrscht Konsensus, wie bei vielen Gliedern einer Kette, die miteinander zusammen hängen, und wo jeder einen eigenen Platz hat.
Der Wechsel zwischen Rang und Hierarchie schafft viele Gedankenfehler, wenn wir glauben, dass nur weil etwas schnell und effektiv scheint, dann ist es auch gut.

Ja, kurzfristig gewinnen wir Zeit.
Aber nein, langfristig bekommen wir Probleme mit den Folgen von solchen Dingen wie Schnellgehen.
Es ist nur in unserer jetzigen Zeit, wo es wichtig geworden ist, Zeit zu sparen, wo es einem möglich ist, das zu tun.
Leider.
Es ist nicht möglich.
Das Leben bewegt sich vorwärts, auf die gleiche Weise die ganze Zeit, in einem Kontinuum.
So, es ist ganz einfach nicht möglich, Zeit zu sparen.
Genauso wenig wie es möglich ist, den verlorenen Schlaf nachzuholen.
Die Zeit bewegt sich, ob wir das wollen oder nicht, oder sie manipulieren.

<div class="handwritten">
Was sagst du da?
Klar können wir Zeit gewinnen.
Es ist ja lediglich, dass wir Prioritäten setzen, und wir wählen was aus.
Oder was meinst du eigentlich?
</div>

Zeit ist nur eine Vorwärtsbewegung.
Die Zeit bewegt sich.
Und sie bewegt sich unabhängig von dem, was du tust.
Sekundärerweise kannst du die Zeit bewerten und meinen, du verlierst Zeit, oder du gewinnst Zeit, aber in Wirklichkeit hast du immer alle Zeit, die ganze Zeit, dein ganzes Leben lang.

Du kannst nichts anderes mit ihr unternehmen als da sein.
Und wenn du klug bist, bist du dir jedes Augenblicks bewusst, und da bereichert es dein inneres Leben und dies egal, ob du still bist oder dich bewegst.
Egal, ob du etwas tust oder einfach dasitzt und dir Überlegungen anstellst.

Was auch deine Erinnerung bereichert ist all das, was du gerne tust.
Wenn du im Leben genießt, dann hast du einen unglaublichen Reichtum, den du hervorholen kannst,über den du denken kannst, über den du reden kannst, den du erzählen kannst und den du genießen kannst.
Vor allem im Beisammensein mit anderen Menschen.

<div class="handwritten">
Das werde ich mir überlegen.
</div>
