## Warum ist Umdenken so schwer?

<div class="handwritten">
Ich habe so ein starkes Bild davon wie es sein soll und ich erwarte, dass andere das einsehen sollen und sich anschließen sollen.
Und wenn das nicht so kommt, dann werde ich ganz verwirrt und ich werfe es ihnen vor, weil ich so enttäuscht werde.
Ich bemerke es erst hinterher, dass ich dies bin.
</div>

In dem, was du sagst, liegt, dass wir nicht trainiert werden darin, wie wir umdenken können wenn wir im Alter von vier bis acht Jahren sind.
Es ist die Zeit, wo diese Fähigkeit heranreift.
Die meisten Erwachsenen haben selber nicht lernen können wie man umdenkt, sondern sind stattdessen manipuliert worden oder gezwungen worden zur Anpassung.
Und da werden wir oft als Erwachsene an eine gewisse Weise gebunden und jedenfalls werden wir da nicht besonders gut unseren Kindern beizubringen, wie man umdenkt.

Wenn wir dann befallen werden durch die Unvollständigkeiten des Lebens, wenn wir klein sind, und wir haben diese äußere Geborgenheit nicht bekommen nachdem wir geboren sind, das, was bewirkt, dass wir in Geborgenheit verbleiben können, in dieser Geborgenheit darauf, dass das Äußere ungefährlich ist, dann wird der Körper ängstlich, dann werden wir meinen, dass etwas lebensgefährlich ist, was im Grunde ungefährlich ist.
Aber wir erleben bedrohliche Angst, abgewiesen und verlassen zu werden.
Dann werden wir hineingezogen in das sekundäre Bewerten und da finden wir etwas, um dem zu entkommen, was uns bedroht, dem zu entkommen, was wir als gefährlich erleben, die Aggressivität anderer Menschen und ihre Unzufriedenheit ihre Abweisung, dass wir allein gelassen werden und so weiter.

Ich pflege es zu nennen, dass wir ein Schutz- und Verteidigungssystem haben, einen Autopiloten.
Dieser ist notwendig, wenn wir wirklich in Lebensgefahr sind.
Und dieser Autopilot geht nicht über unseren Intellekt.
Er geht aus (von???) unserem Reptilhirn, dem Hirn, das die autonomen Überlebenssysteme steuert, die in unserer Konstitution sind.
So steuern in der Richtung steuern, dass wir immer überleben, egal wie schlecht unsere Chancen sind.
Wenn wir wirklich in Gefahr sind, da ist es wichtig, dass wir uns sofort anpassen können, da bleibt uns keine Zeit zum Überlegen, sondern nur zum Reagieren.
Und dann dieser Stelle ist es für das Überleben notwendig.
Diesem Mechanismus bedienen sich Eltern und bedrohen ihre Kinder, so dass ihnen keine Zeit bleibt zum Reflektieren und zum autonom Reagieren.
Sondern sie passen sich stattdessen an.
Und dies ist schnell effektiv und rational und bequem.
Und es scheint konstruktiv, aber im Grunde ist es verheerend für denjenigen oder für diejenige, der oder die
diesem ausgesetzt wird.
Dies macht, dass diese ganze Zivilisation, die meisten da, ihnen geht es mehr oder weniger schlecht, und sie meinen, dass sie sich anpassen müssen, um dabei sein zu dürfen.
Anpassen, damit sie nicht ausgeschlossen werden.
Diese ständige Angst zwingt die Leute in Anpassung und zwingt sie, Verantwortung zu übernehmen, wie wenn Verantwortung ein Produkt wäre, dass man übernehmen kann.

Anstatt die Freiheit zu leben darin verantwortlich alles tun zu können, was wir tun können.
Und die Freude darin spüren, wie es ist, wenn wir etwas können.
Wenn wir hingegen nicht in Lebensgefahr sind, aber meinen, wir seien es, so fällt es uns leicht, das Gleiche zu tun, uns anzupassen, und da werden Vorstellungen Ideale und Ziele geschaffen, die wir erreichen wollen.
Wir bedienen uns ihnen, um nicht verantwortlich und frei leben zu müssen, sondern etwas haben, dem wir die Schuld geben können.
Wir unterordnen uns etwas oder jemanden, der verantwortlich ist und dem wir die Schuld geben können.
Oder wir bewirken Unvollständigkeiten, weshalb wir nicht perfekt sind.
Da kann man von einem großen Majoritätsmissverständnis sprechen.
Es umfasst fast alle in der Zivilisation.
